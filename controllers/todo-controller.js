const TodoController = {};

TodoController.index = async(req, res, next) => {
    res.send([{name: "Alice Todo 1"}, {name: "Alice Todo 2"}]);
};

TodoController.show = async(req, res, next) => {
    res.send({name: "Alice Todo 1 Details"});
};

TodoController.create = async(req, res, next) => {};

TodoController.update = async(req, res, next) => {};

TodoController.destroy = async(req, res, next) => {};

module.exports = TodoController;
