const UserController = {};


UserController.index = async(req, res, next) => {
    res.send([{name: "Alice"}, {name: "Bob"}]);
};

UserController.show = async(req, res, next) => {
    res.send({name: "Alice"});
};

UserController.create = async(req, res, next) => {};

UserController.update = async(req, res, next) => {};

UserController.destroy = async(req, res, next) => {};

module.exports = UserController;
