var express = require('express');
var router = express.Router();

const TodoController = require('../controllers/todo-controller.js');

router.get('/',     TodoController.index);
router.get('/:id',  TodoController.show);
router.post('/',    TodoController.create);
router.put('/:id',  TodoController.update);
router.delete('/:id',  TodoController.destroy);

module.exports = router;
