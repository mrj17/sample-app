var express = require('express');
var router = express.Router();

const UserController = require('../controllers/user-controller.js');

const TodoRoutes = require('./todo-routes');

router.get('/',     UserController.index);
router.get('/:id',  UserController.show);
router.post('/',    UserController.create);
router.put('/:id',  UserController.update);
router.delete('/:id',  UserController.destroy);

router.use('/:id/todos', TodoRoutes);

module.exports = router;
